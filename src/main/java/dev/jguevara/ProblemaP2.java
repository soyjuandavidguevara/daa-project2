package dev.jguevara;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

enum DataType {
  W1,
  W2,
  NUMBER_OF_ELEMENTS,
  ELEMENT_A,
  ELEMENT_B,
}

class Pair<T, E> {
  private T first;
  private final E second;

  public Pair(T first, E second) {
    this.first = first;
    this.second = second;
  }

  public T first() {
    return first;
  }

  public void first(T first) {
    this.first = first;
  }

  public E second() {
    return second;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Pair<?, ?> pair = (Pair<?, ?>) o;
    return (first.equals(pair.first) && second.equals(pair.second)) || (first.equals(pair.second) && second.equals(pair.first));
  }

  @Override
  public String toString() {
    return String.format("(%s,%s)", first.toString(), second.toString());
  }

  @Override
  public int hashCode() {
    int hash1 = first.hashCode();
    int hash2 = second.hashCode();
    return 31 * (hash1 + hash2) + (hash1 * hash2);
  }
}

class Loader {
  private final Scanner scanner;
  private int testCases = 0;
  private int current = 0;

  public Loader(String location) throws FileNotFoundException {
    this.scanner = new Scanner(new FileReader(location));
  }

  public void init() {
    scanner.reset();
    this.testCases = scanner.nextInt();
  }

  public void close() {
    scanner.close();
  }

  public DataIterator readNextCase() {
    current += 1;
    return new DataIterator(scanner);
  }

  public boolean missingCase() {
    return current < testCases;
  }
}

class Data {
  private final DataType type;
  private final int value;

  public Data(DataType dataType, int value) {
    this.type = dataType;
    this.value = value;
  }

  public DataType getType() {
    return type;
  }

  public int getValue() {
    return value;
  }

  @Override
  public String toString() {
    return type.name() + ": " + value;
  }
}


class DataIterator implements Iterator<Data> {
  private final Scanner scanner;
  private Data current;
  private int missing = 0;

  public DataIterator(Scanner scanner) {
    this.scanner = scanner;
  }

  @Override
  public boolean hasNext() {
    if (current == null || current.getType() == DataType.NUMBER_OF_ELEMENTS) return true;
    return missing > 0;
  }

  @Override
  public Data next() {
    if (current == null) {
      current = new Data(DataType.NUMBER_OF_ELEMENTS, scanner.nextInt());
      missing = current.getValue();
    } else if (current.getType() == DataType.NUMBER_OF_ELEMENTS) {
      current = new Data(DataType.W1, scanner.nextInt());
    } else if (current.getType() == DataType.W1) {
      current = new Data(DataType.W2, scanner.nextInt());
    } else if (current.getType() == DataType.W2) {
      current = new Data(DataType.ELEMENT_A, scanner.nextInt());
    } else if (current.getType() == DataType.ELEMENT_A) {
      current = new Data(DataType.ELEMENT_B, scanner.nextInt());
      missing -= 1;
    } else if (current.getType() == DataType.ELEMENT_B) {
      current = new Data(DataType.ELEMENT_A, scanner.nextInt());
    }
    return current;
  }
}


class FundamentalElement {
  private Pair<Integer, Integer> pair;

  public FundamentalElement(int a, int b) {
    this.pair = new Pair<>(a, b);
  }

  public int getFirst() {
    return this.pair.first();
  }

  public int getSecond() {
    return this.pair.second();
  }

  public void swap() {
    this.pair = new Pair<>(this.pair.second(), this.pair.first());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FundamentalElement that = (FundamentalElement) o;
    return that.pair.equals(this.pair);
  }

  @Override
  public int hashCode() {
    return this.pair.hashCode();
  }

  @Override
  public String toString() {
    return "(" + getFirst() +
        "," + getSecond() + ")";
  }
}

class FreeAtomsMST {
  private final Map<Integer, Map<Integer, Integer>> distances = new HashMap<>();
  private final Map<Integer, Map<Integer, List<Integer>>> paths = new HashMap<>();
  private final Set<Integer> availableAtoms = new HashSet<>();
  private final int w1;
  private final int w2;

  public FreeAtomsMST(int w1, int w2, Set<Integer> atoms) {
    this.w1 = w1;
    this.w2 = w2;
    for (int atomI : atoms) {
      distances.put(atomI, new HashMap<>());
      distances.put(-atomI, new HashMap<>());
      availableAtoms.add(atomI);
      availableAtoms.add(-atomI);
      paths.put(atomI, new HashMap<>());
      paths.put(-atomI, new HashMap<>());
    }
  }

  // Uses Floyd Algorithm to calculate paths
  public void calculateWeights() {
    for (int atomI : availableAtoms) {
      for (int atomJ : availableAtoms) {
        if (Math.abs(atomI) == Math.abs(atomJ)) {
          distances.get(atomI).put(atomJ, Integer.MAX_VALUE);
          paths.get(atomI).put(atomJ, new ArrayList<>(List.of(atomI, atomJ)));
        } else {
          boolean firstNegative = atomI < 0;
          boolean secondNegative = atomJ < 0;
          int ltp;
          int abs = Math.abs(Math.abs(atomI) - Math.abs(atomJ));
          if ((firstNegative && secondNegative) || !(firstNegative || secondNegative)) {
            ltp = 1 + (abs % w1);
          } else {
            ltp = w2 - (abs % w2);
          }
          distances.get(atomI).put(atomJ, ltp);
          paths.get(atomI).put(atomJ, new ArrayList<>(List.of(atomI, atomJ)));
        }
      }
    }

    for (int atomI : availableAtoms) {
      for (int atomJ : availableAtoms) {
        for (int atomK : availableAtoms) {
          if (distances.get(atomI).get(atomK) == Integer.MAX_VALUE) {
            continue;
          }
          if (distances.get(atomK).get(atomJ) == Integer.MAX_VALUE) {
            continue;
          }
          if (atomI == atomJ) continue;

          if (distances.get(atomI).get(atomJ) > distances.get(atomI).get(atomK) + distances.get(atomK).get(atomJ)) {
            distances.get(atomI).put(atomJ, distances.get(atomI).get(atomK) + distances.get(atomK).get(atomJ));
            Set<Integer> newPath = new LinkedHashSet<>();
            newPath.addAll(paths.get(atomI).get(atomK));
            newPath.addAll(paths.get(atomJ).get(atomJ));
            paths.get(atomI).put(atomJ, new LinkedList<>(newPath));
          }
        }
      }
    }
  }

  public int getDistance(FundamentalElement from, FundamentalElement to) {
    int common = from.getFirst() == to.getFirst() || from.getFirst() == to.getSecond() ? from.getFirst() : to.getSecond();
    return distances.get(-common).get(common);
  }

  public int getTotalDistance(List<FundamentalElement> path) {
    int total = 0;
    for (int i = 0; i < path.size() - 1; i++) {
      total += getDistance(path.get(i), path.get(i + 1));
    }
    return total;
  }

  public List<Integer> getConnection(FundamentalElement from, FundamentalElement to) {
    int common = from.getFirst() == to.getFirst() || from.getFirst() == to.getSecond() ? from.getFirst() : from.getSecond();

    if (distances.get(-common).get(common) == Integer.MAX_VALUE)
      return null;

    return paths.get(-common).get(common);
  }
}

class Graph {
  private final Map<FundamentalElement, Set<FundamentalElement>> adjList = new HashMap<>();

  public void addElement(FundamentalElement fundamentalElement) {
    if (adjList.containsKey(fundamentalElement)) return;
    Set<FundamentalElement> neighbors = new HashSet<>();
    for (FundamentalElement existent : adjList.keySet()) {
      if (fundamentalElement.getFirst() == existent.getFirst() || fundamentalElement.getFirst() == existent.getSecond()) {
        neighbors.add(existent);
        adjacent(existent).add(fundamentalElement);
      } else if (fundamentalElement.getSecond() == existent.getFirst() || fundamentalElement.getSecond() == existent.getSecond()) {
        neighbors.add(existent);
        adjacent(existent).add(fundamentalElement);
      }
    }
    adjList.put(fundamentalElement, neighbors);
  }

  public Set<FundamentalElement> vertices() {
    return this.adjList.keySet();
  }

  public boolean connected() {
    Set<FundamentalElement> visited = new HashSet<>();
    Queue<FundamentalElement> queue = new LinkedList<>();

    FundamentalElement start = adjList.keySet().iterator().next();

    queue.add(start);
    visited.add(start);

    while (!queue.isEmpty()) {
      FundamentalElement head = queue.poll();

      for (FundamentalElement neigh : this.adjacent(head)) {
        if (!visited.contains(neigh)) {
          visited.add(neigh);
          queue.add(neigh);
        }
      }
    }

    return visited.size() == adjList.size();
  }

  private List<FundamentalElement> searchDFS(FundamentalElement from, Set<FundamentalElement> visited) {
    visited.add(from);

    if (visited.size() == adjList.size()) {
      return new LinkedList<>(visited);
    }

    List<FundamentalElement> neighbors = new ArrayList<>(this.adjacent(from)
        .stream()
        .sorted(Comparator.comparing(it -> this.adjacent(it).stream().filter(n -> !visited.contains(n)).count()))
        .toList());

    for (FundamentalElement neigh : neighbors) {
      if (visited.contains(neigh)) continue;  // Already visited

      // Recursive call with a copy of the visited set for each branch
      List<FundamentalElement> result = searchDFS(neigh, new LinkedHashSet<>(visited));
      if (result != null) {
        return result;  // Found a branch from the neighbor, return it
      }
    }

    // No branch found from this node, remove it from visited (backtracking)
    visited.remove(from);
    return null;
  }

  private void dfs(FundamentalElement from, int distance, Set<FundamentalElement> visited, Map<FundamentalElement, Integer> distances) {
    visited.add(from);
    distances.put(from, distance);

    for (FundamentalElement neigh : this.adjacent(from)) {
      if (visited.contains(neigh)) continue;

      dfs(neigh, distance + 1, visited, distances);
    }
  }

  private FundamentalElement getFarthestVertex(FundamentalElement from) {
    FundamentalElement farthestVertex = from;
    int farthestDistance = Integer.MIN_VALUE;

    Map<FundamentalElement, Integer> distances = new HashMap<>(adjList.size());

    dfs(from, 0, new HashSet<>(adjList.size()), distances);

    for (FundamentalElement element: vertices()) {
      if (distances.get(element) > farthestDistance) {
        farthestDistance = distances.get(element);
        farthestVertex = element;
      }
    }

    return farthestVertex;
  }


  // If there are one or two 1-grade vertex, we have to start in one of them.
  // Otherwise, we initialize a DFS in any node k.
  // The further node to k will be the starting point.
  public Pair<List<FundamentalElement>, Integer> getConnection(FreeAtomsMST freeAtomsMST) {
    FundamentalElement desiredStart = Collections.min(this.vertices(), Comparator.comparing(it -> adjacent(it).size()));

    if (adjacent(desiredStart).size() > 1) {
      desiredStart = getFarthestVertex(desiredStart);
    }

    List<FundamentalElement> path = searchDFS(
      desiredStart,
      new LinkedHashSet<>()
    );

    if (path == null) return null;

    return new Pair<>(path, freeAtomsMST.getTotalDistance(path));
  }

  public boolean matchesRequirements() {
    int withGrade1 = 0;
    int maximum = 0;
    int minimum = Integer.MAX_VALUE;

    if (!connected()) return false;

    for (FundamentalElement elementA : adjList.keySet()) {
      if (withGrade1 > 2) return false;
      if (adjacent(elementA).isEmpty()) return false;
      maximum = Math.max(adjacent(elementA).size(), maximum);
      minimum = Math.min(adjacent(elementA).size(), minimum);
      if (adjacent(elementA).isEmpty()) return false;
      if (adjacent(elementA).size() == 1) {
        withGrade1 += 1;
      }
    }
    return withGrade1 <= 2;
  }

  public Set<FundamentalElement> adjacent(FundamentalElement element) {
    return this.adjList.get(element);
  }
}

class Ultrasense {
  private final dev.jguevara.graphs.Graph gr = new dev.jguevara.graphs.Graph();
  private final Set<Integer> availableAtoms = new HashSet<>();
  private final List<int[]> fundamentalElements = new ArrayList<>();
  private final int[] w = new int[2];

  public void registerFundamentalElement(int[] element) {
    this.fundamentalElements.add(element);
  }

  public void registerAvailableAtom(int atom) {
    this.availableAtoms.add(Math.abs(atom));
  }

  public void weight(int idx, int value) {
    w[idx - 1] = value;
  }

  public List<FundamentalElement> orderConnections(List<FundamentalElement> pairs) {
    if (pairs.size() == 1) return pairs;
    for (int i = 0; i < pairs.size(); i++) {
      if (i == 0) {
        // Shared must be the second
        FundamentalElement current = pairs.get(0);
        FundamentalElement next = pairs.get(1);

        if (current.getFirst() == next.getFirst() || current.getFirst() == next.getSecond())
          current.swap();
      } else {
        FundamentalElement previousPair = pairs.get(i - 1);
        FundamentalElement currentPair = pairs.get(i);

        if (previousPair.getSecond() != currentPair.getFirst()) {
          currentPair.swap();
        }
      }
    }

    return pairs;
  }

  public String connectElements() {
    if (fundamentalElements.size() == 1) return String.format("(%s,%s) 0", fundamentalElements.get(0)[0], fundamentalElements.get(0)[1]);
    for (int[] element : fundamentalElements) {
      gr.addElement(new FundamentalElement(element[0], element[1]));
    }

    if (!gr.matchesRequirements()) return "NO SE PUEDE";
    FreeAtomsMST freeAtomsMST = new FreeAtomsMST(w[0], w[1], availableAtoms);
    freeAtomsMST.calculateWeights();

    Pair<List<FundamentalElement>, Integer> best = gr.getConnection(freeAtomsMST);

    if (best == null) return "NO SE PUEDE";

    List<FundamentalElement> chain = orderConnections(best.first());

    StringBuilder result = new StringBuilder();

    for (int i = 0; i < chain.size() - 1; i++) {
      FundamentalElement current = chain.get(i);
      FundamentalElement next = chain.get(i + 1);

      result.append(current);
      result.append(",");

      List<Integer> freeAtomsChain = freeAtomsMST.getConnection(current, next);

      for (int j = 0; j < freeAtomsChain.size() - 1; j++) {
        result.append(freeAtomsChain.get(j));
        result.append(",");
      }
    }

    result.append(chain.get(chain.size() - 1));
    result.append(" ");
    result.append(best.second());

    return result.toString();
  }
}


public class ProblemaP2 {
  public static void main(String[] args) throws IOException {
    Loader loader = new Loader("P1.in");
    FileWriter output = new FileWriter("P1.out");
    loader.init();
    int i = 0;
    while (loader.missingCase()) {
      DataIterator it = loader.readNextCase();
      Ultrasense ultrasense = new Ultrasense();
      int elementA = 0;
      int size = 0;
      while (it.hasNext()) {
        Data part = it.next();
        if (part.getType() == DataType.NUMBER_OF_ELEMENTS) {
          size = part.getValue();
        } else if (part.getType() == DataType.W1) {
          ultrasense.weight(1, part.getValue());
        } else if (part.getType() == DataType.W2) {
          ultrasense.weight(2, part.getValue());
        } else if (part.getType() == DataType.ELEMENT_A) {
          elementA = part.getValue();
          ultrasense.registerAvailableAtom(part.getValue());
        } else if (part.getType() == DataType.ELEMENT_B) {
          ultrasense.registerAvailableAtom(part.getValue());
          ultrasense.registerFundamentalElement(new int[]{elementA, part.getValue()});
        }
      }
      System.out.printf("Doing for %s fundamental elements\n", size);
      output.write(ultrasense.connectElements() + "\n");
      output.flush();
      i += 1;
      System.out.printf("Finished case %s\n", i);
    }
    output.close();
    loader.close();
  }
}
