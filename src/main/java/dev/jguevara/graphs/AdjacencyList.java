package dev.jguevara.graphs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdjacencyList<T> extends Graph<T> {
  private final Map<T, List<Edge<T>>> adjacencyList;

  public AdjacencyList(String name) {
    super(name);
    this.adjacencyList = new HashMap<>();
  }

  public AdjacencyList(String name, int capacity) {
    super(name);
    this.adjacencyList = new HashMap<>(capacity);
  }

  @Override
  public boolean addVertex(T vertex) {
    return false;
  }

  @Override
  public boolean addEdge(T v, T w, Number weight) {
    return false;
  }

  @Override
  public List<T> vertices() {
    return List.of();
  }

  @Override
  public List<Edge<T>> edges() {
    return List.of();
  }

  @Override
  public List<T> adjacentVertices(T v) {
    return List.of();
  }

  @Override
  public boolean hasVertex(T vertex) {
    return false;
  }

  @Override
  public boolean hasEdge(T v, T w) {
    return false;
  }

  @Override
  public boolean removeVertex(T vertex) {
    return false;
  }

  @Override
  public boolean removeEdge(T v, T w) {
    return false;
  }
}
