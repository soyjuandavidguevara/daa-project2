package dev.jguevara.graphs;

import dev.jguevara.Configuration;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * The Graph class represents an abstract graph data structure.
 *
 * @param <T> the type of vertices in the graph
 */
public abstract class Graph<T> {
  protected int size;
  protected String name;

  protected Graph(String name) {
    this.name = name;
    this.size = 0;
  }

  /**
   * Adds a vertex to the graph.
   *
   * @param vertex the vertex to be added to the graph
   * @return true if the vertex is successfully added, false otherwise
   */
  public abstract boolean addVertex(T vertex);
  /**
   * Adds an edge to the graph with the specified weight.
   *
   * @param v the first vertex of the edge
   * @param w the second vertex of the edge
   * @param weight the weight of the edge
   * @return true if the edge is successfully added, false otherwise
   */
  public abstract boolean addEdge(T v, T w, Number weight); // Ttu

  /**
   * Returns a list of vertices in the graph.
   *
   * @return a list of vertices in the graph
   */
  public abstract List<T> vertices();

  /**
   * Returns a list of edges in the graph.
   *
   * @return a list of edges in the graph
   */
  public abstract List<Edge<T>> edges();

  /**
   * Returns a list of adjacent vertices to the given vertex.
   *
   * @param v the vertex to find the adjacent vertices for
   * @return a list of adjacent vertices to the given vertex
   */
  public abstract List<T> adjacentVertices(T v);
  /**
   * Checks if the graph contains the given vertex.
   *
   * @param vertex the vertex to check if it is in the graph
   * @return true if the graph contains the vertex, false otherwise
   */
  public abstract boolean hasVertex(T vertex);
  /**
   * Checks if the graph contains an edge between two given vertices.
   *
   * @param v the first vertex to check for an edge
   * @param w the second vertex to check for an edge
   * @return true if an edge exists between the vertices, false otherwise
   */
  public abstract boolean hasEdge(T v, T w);

  /**
   * Removes a vertex from the graph.
   *
   * @param vertex the vertex to be removed from the graph
   * @return true if the vertex is successfully removed, false otherwise
   */
  public abstract boolean removeVertex(T vertex);
  /**
   * Removes an edge between two vertices in the graph.
   *
   * @param v the first vertex of the edge to be removed
   * @param w the second vertex of the edge to be removed
   * @return true if the edge is successfully removed, false otherwise
   */
  public abstract boolean removeEdge(T v, T w);

  /**
   * Creates a visualization of the graph in DOT format and saves it to a file.
   *
   * @throws IOException if an I/O error occurs
   */
  public void createVisualization() throws IOException {
    FileWriter writer = new FileWriter(Configuration.VISUALIZATION_FOLDER + this.name + ".dot");

    writer.close();
  }
}
