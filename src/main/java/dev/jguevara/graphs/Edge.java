package dev.jguevara.graphs;

public record Edge<T>(T source, T destination, Number weight) {
  public T either() {
    return source;
  }

  public T other(T either) {
    if (source.equals(either)) return destination;
    return source;
  }
}
